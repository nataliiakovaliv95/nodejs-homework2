const User = require('../models/user');
const Note = require('../models/note');
const passwordService = require('../services/passwordService');
const validationService = require('../services/validationService');

const getUserInfo = (req, res) => {
  const user = req.decoded;
  res.status(200).json({
    user: {
      _id: user._id,
      username: user.username,
      createdDate: user.createdDate,
    },
  });
};

const deleteUserProfile = async (req, res) => {
  const user = req.decoded;
  try {
    await User.deleteUser(user._id);
    await Note.deleteUsersNotes(user._id);
    res.status(200).json({message: 'Success'});
  } catch (err) {
    res.status(500).json({message: 'Server error'});
    console.log(err);
  }
};

const changeUserPassword = async (req, res) => {
  const user = req.decoded;
  let {oldPassword, newPassword} = req.body;
  try {
    const err = await validationService.userReqValidation(
        oldPassword, newPassword, user.username,
    );
    if (err) {
      res.status(400).json({message: err});
    } else {
      newPassword = await passwordService.cryptPassword(newPassword);
      await User.changeUserPassword(user._id, newPassword);
      res.status(200).json({message: 'Success'});
    }
  } catch (err) {
    res.status(500).json({message: 'Server error'});
    console.log(err);
  }
};

module.exports = {
  getUserInfo,
  deleteUserProfile,
  changeUserPassword,
};
