const User = require('../models/user');
const passwordService = require('../services/passwordService');
const validationService = require('../services/validationService');
const tokenService = require('../services/tokenService');

const registerUser = async (req, res) => {
  const {username, password} = req.body;
  const url = req.url;
  try {
    const err = await validationService.authReqValidation(
        username, password, url,
    );
    if (err) {
      res.status(400).json({message: err});
    } else {
      const newUser = new User({
        username,
        password: await passwordService.cryptPassword(password),
      });
      await User.addUser(newUser);
      res.status(200).json({message: 'Success'});
    }
  } catch (err) {
    res.status(500).json({message: 'Server error'});
    console.log(err);
  }
};

const loginUser = async (req, res) => {
  const {username, password} = req.body;
  const url = req.url;
  try {
    const err = await validationService.authReqValidation(
        username, password, url,
    );
    if (err) {
      res.status(400).json({message: err});
    } else {
      const user = await User.getUserByUsername(username);
      const correctPassword = await passwordService.comparePasswords(
          password, user.password,
      );
      if (correctPassword) {
        const token = tokenService.signToken(user.toJSON());
        res.status(200).json({
          message: 'Success',
          jwt_token: token,
        });
      } else {
        res.status(400).json({message: 'Wrong password'});
      }
    }
  } catch (err) {
    res.status(500).json({message: 'Server error'});
    console.log(err);
  }
};

module.exports = {
  registerUser,
  loginUser,
};
