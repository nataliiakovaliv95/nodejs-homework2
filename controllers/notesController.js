const Note = require('../models/note');
const validationService = require('../services/validationService');

const getNotes = async (req, res) => {
  const offset = +req.query.offset || 0;
  const limit = +req.query.limit || 0;
  const user = req.decoded;
  const userId = user._id;
  try {
    const allNotes = await Note.getNotes(userId);
    const notes = await Note.getNotesByLimit(userId, offset, limit);
    const count = limit ? Math.ceil(allNotes.length / limit) : 0;
    res.status(200).json({
      offset,
      limit,
      count,
      notes,
    });
  } catch (err) {
    res.status(500).json({message: 'Server error'});
    console.log(err);
  }
};

const addNote = async (req, res) => {
  const user = req.decoded;
  const text = req.body.text;
  try {
    const err = validationService.noteReqTextValidation(text);
    if (err) {
      res.status(400).json({message: err});
    } else {
      const newNote = new Note({
        userId: user._id,
        text,
      });
      await Note.saveNote(newNote);
      res.status(200).json({message: 'Success'});
    }
  } catch (err) {
    res.status(500).json({message: 'Server error'});
    console.log(err);
  }
};

const getNote = async (req, res) => {
  const id = req.params.id;
  try {
    const err = await validationService.noteReqIdValidation(id);
    if (err) {
      res.status(400).json({message: err});
    } else {
      const note = await Note.getNoteById(id);
      res.status(200).json({note});
    }
  } catch (err) {
    res.status(500).json({message: 'Server error'});
    console.log(err);
  }
};

const updateNote = async (req, res) => {
  const text = req.body.text;
  const id = req.params.id;
  try {
    const err = validationService.noteReqTextValidation(text) ||
    await validationService.noteReqIdValidation(id);
    if (err) {
      res.status(400).json({message: err});
    } else {
      await Note.changeNoteText(id, text);
      res.status(200).json({message: 'Success'});
    }
  } catch (err) {
    res.status(500).json({message: 'Server error'});
    console.log(err);
  }
};

const updateNoteStatus = async (req, res) => {
  const id = req.params.id;
  try {
    const err = await validationService.noteReqIdValidation(id);
    if (err) {
      res.status(400).json({message: err});
    } else {
      const note = await Note.getNoteById(id);
      const completed = !note.completed;
      await Note.changeNoteStatus(id, completed);
      res.status(200).json({message: 'Success'});
    }
  } catch (err) {
    res.status(500).json({message: 'Server error'});
    console.log(err);
  }
};

const deleteNote = async (req, res) => {
  const id = req.params.id;
  try {
    const err = await validationService.noteReqIdValidation(id);
    if (err) {
      res.status(400).json({message: err});
    } else {
      await Note.deleteNote(id);
      res.status(200).json({message: 'Success'});
    }
  } catch (err) {
    res.status(500).json({message: 'Server error'});
    console.log(err);
  }
};

module.exports = {
  getNotes,
  addNote,
  getNote,
  updateNote,
  updateNoteStatus,
  deleteNote,
};
