const express = require('express');
const router = new express.Router();
const userController = require('../controllers/userController');
const tokenService = require('../services/tokenService');

router.get('/', tokenService.verifyToken, userController.getUserInfo);

router.delete('/', tokenService.verifyToken, userController.deleteUserProfile);

router.patch('/', tokenService.verifyToken, userController.changeUserPassword);


module.exports = router;
