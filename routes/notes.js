const express = require('express');
const router = new express.Router();
const notesController = require('../controllers/notesController');
const tokenService = require('../services/tokenService');

router.get('/', tokenService.verifyToken, notesController.getNotes);

router.post('/', tokenService.verifyToken, notesController.addNote);

router.get('/:id', tokenService.verifyToken, notesController.getNote);

router.put('/:id', tokenService.verifyToken, notesController.updateNote);

router.patch(
    '/:id', tokenService.verifyToken, notesController.updateNoteStatus,
);

router.delete('/:id', tokenService.verifyToken, notesController.deleteNote);

module.exports = router;
