const passwordService = require('../services/passwordService');
const User = require('../models/user');
const Note = require('../models/note');

const authReqValidation = async (username, password, url) => {
  if (!username) {
    return 'Please specify \'username\' parameter';
  } else if (!password) {
    return 'Please specify \'password\' parameter';
  } else {
    const user = await User.getUserByUsername(username); {
      if (url === '/register' && user) {
        return 'You already have an account';
      } else if (url === '/login' && !user) {
        return 'User not found';
      }
    }
  }
};

const userReqValidation = async (oldPassword, newPassword, username) => {
  if (!oldPassword) {
    return 'Please specify \'oldPassword\' parameter';
  } else if (!newPassword) {
    return 'Please specify \'newPassword\' parameter';
  } else if (oldPassword === newPassword) {
    return 'New password must be different';
  } else {
    const user = await User.getUserByUsername(username);
    const correctPassword = await passwordService.comparePasswords(
        oldPassword, user.password,
    );
    if (!correctPassword) {
      return 'Wrong Password';
    }
  }
};

const noteReqIdValidation = async (id) => {
  if (id.length !== 24) {
    return 'Id is not correct';
  } else {
    const note = await Note.getNoteById(id);
    if (!note) {
      return 'Note not found';
    }
  }
};

const noteReqTextValidation = (text) => {
  if (!text) {
    return 'Please specify \'text\' parameter';
  }
};

module.exports = {
  authReqValidation,
  userReqValidation,
  noteReqIdValidation,
  noteReqTextValidation,
};
