const {Schema, model} = require('mongoose');

const userSchema = new Schema({
  username: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
  createdDate: {
    type: Date,
    default: new Date().toString(),
  },
}, {versionKey: false});

const User = module.exports = model('User', userSchema);

module.exports.getUserByUsername = (username) => {
  return User.findOne({username: username});
};

module.exports.addUser = (user) => user.save();

module.exports.deleteUser = (id) => User.findByIdAndRemove(id);

module.exports.changeUserPassword = (_id, password) => {
  return User.findByIdAndUpdate({_id}, {password});
};
