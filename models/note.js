const {Schema, model} = require('mongoose');

const noteSchema = new Schema({
  userId: {
    type: String,
    required: true,
  },
  completed: {
    type: Boolean,
    default: false,
  },
  text: {
    type: String,
    required: true,
  },
  createdDate: {
    type: Date,
    default: new Date().toString(),
  },
}, {versionKey: false});

const Note = module.exports = model('Note', noteSchema);

module.exports.getNoteById = (id) => Note.findById(id);

module.exports.saveNote = (note) => note.save();

module.exports.deleteNote = (id) => Note.findByIdAndRemove(id);

module.exports.changeNoteText = (_id, text) => {
  return Note.findByIdAndUpdate({_id}, {text});
};

module.exports.changeNoteStatus = (_id, completed) => {
  return Note.findByIdAndUpdate({_id}, {completed});
};

module.exports.getNotes = (userId) => Note.find({userId});

module.exports.getNotesByLimit = (userId, offset, limit) => {
  return Note.find({userId}).skip(offset).limit(limit);
};

module.exports.deleteUsersNotes = (userId) => Note.deleteMany({userId});
